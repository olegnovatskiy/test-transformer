<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    \TransformerTextBundle\TransformerTextBundle::class => ['all' => true],
];
