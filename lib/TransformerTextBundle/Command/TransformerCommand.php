<?php

namespace TransformerTextBundle\Command;

use TransformerTextBundle\Interfaces\TextTransformerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Class TransformerCommand
 */
class TransformerCommand extends Command
{
    private const TASK_EXIT          = 0;
    private const TASK_REMOVING_HTML = 1;
    private const TASK_WRAP_OUT_TO_P = 2;

    private const TEXT_FROM_CONSOLE = 1;
    private const TEXT_FROM_FILE    = 2;

    /**
     * @var string $defaultName
     */
    protected static $defaultName = 'text:process';

    /**
     * @var QuestionHelper $questionHelper
     */
    private $questionHelper;

    /**
     * @var TextTransformerInterface $transformerService
     */
    private $transformerService;


    /**
     * TransformerCommand constructor.
     *
     * @param string|null              $name
     * @param TextTransformerInterface $transformerService
     */
    public function __construct(
        ?string $name = null,
        TextTransformerInterface $transformerService
    ) {
        parent::__construct($name);

        $this->transformerService = $transformerService;
    }

    /**
     * Method that execute logic of this command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    public function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $taskType = $this->enterTaskType($input, $output);
        if (self::TASK_EXIT === $taskType) {
            $output->writeln(htmlentities('<p>'));

            return null;
        }

        $handledText = $this->enterHandleText($input, $output);

        $result = 'Undefined command.';
        if (self::TASK_REMOVING_HTML === $taskType) {
            $result = $this->transformerService->removeHtml($handledText);
        } elseif (self::TASK_WRAP_OUT_TO_P === $taskType) {
            $result = $this->transformerService->wrapOutParagraph($handledText);
        }

        $output->writeln($result);
    }

    /**
     * Initialize variables used in the rest of the command methods.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->questionHelper = $this->getHelper('question');
    }

    /**
     * Execute choices a task from list.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    private function enterTaskType(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $answers = [
            self::TASK_REMOVING_HTML => 'Exclude HTML tags.',
            self::TASK_WRAP_OUT_TO_P => 'Wrap out to <p> tag.',
            self::TASK_EXIT          => 'Exit.',
        ];

        $taskQuestion = new ChoiceQuestion(
            'Please select a task or exit variant.',
            $answers
        );
        $taskQuestion->setErrorMessage('Task is invalid.');

        $taskType = $this->questionHelper->ask($input, $output, $taskQuestion);

        return array_search($taskType, $answers);
    }

    /**
     * Read a text for processed by transform service.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return string
     */
    private function enterHandleText(
        InputInterface $input,
        OutputInterface $output
    ): string {
        $answers = [
            self::TEXT_FROM_CONSOLE => 'Read from console.',
            self::TEXT_FROM_FILE    => 'Read from TXT file(/path/to/file.txt).',
        ];

        $inputtingQuestion = new ChoiceQuestion(
            'Select a kind of inputting text.',
            $answers
        );
        $inputtingQuestion->setErrorMessage('Kind of inputting is invalid.');
        $inputtingType = $this->questionHelper->ask($input, $output, $inputtingQuestion);
        $inputtingType = array_search($inputtingType, $answers);

        if (self::TEXT_FROM_CONSOLE === $inputtingType) {
            $question = new Question('Please type the text for transformation: ');

            return $this->questionHelper->ask($input, $output, $question);
        }

        return $this->readFileWithText($input, $output);
    }

    /**
     * Read a text from TXT file for precessed by transform service.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return string
     */
    private function readFileWithText(
        InputInterface $input,
        OutputInterface $output
    ): string {
        do {
            $question = new Question(
                'Please type the file name with full path(/home/files/text.txt):'
            );
            $fileName = $this->questionHelper->ask($input, $output, $question);

            if ('.txt' !== mb_substr($fileName, -4)) {
                $output->writeln('Extension of file is not TXT.');
                continue;
            }

            if (false === file_exists($fileName)) {
                $output->writeln('File doesn\'t exist.');
            }
        } while (null === $fileName);

        return file_get_contents($fileName);
    }
}
