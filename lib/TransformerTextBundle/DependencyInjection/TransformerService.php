<?php

namespace TransformerTextBundle\DependencyInjection;

use TransformerTextBundle\Interfaces\TextTransformerInterface;

/**
 * Class TextTransformerService
 */
class TransformerService implements TextTransformerInterface
{


    /**
     * Remove all HTML tags from text.
     *
     * @param string $text
     *
     * @return string
     */
    public function removeHtml(string $text): string
    {
        return strip_tags($text);
    }

    /**
     * Split text into pieces by \n and wrap out them to <p> tag.
     * Before wrapping substrings will be trimmed.
     *
     * @param string $text
     *
     * @return string
     */
    public function wrapOutParagraph(string $text): string
    {
        $lines = explode('\n', $text);

        $lines = array_map('trim', $lines);
        $lines = array_map(
            function ($item) {
                return '<p>' . trim($item) . '</p>';
            },
            $lines
        );

        return implode('', $lines);
    }
}
