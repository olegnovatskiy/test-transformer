
#Text transformer bundle
It provides the service and the console command for editing user text.

The service can do follows:
* Remove all HTML tags from text
* Divided the text into lines that are separated by the help of '\n', then wrapping out them into the tag &lt;p&gt; and again concate them all to the heap.

##Installation
.. configuration-block::

    * Set bundle directory into your project

    * Config autoload for bundle. Example:
    .. code-block:: json

        "autoload": {
            "psr-4": {
                ...
                "TransformerTextBundle\\": "lib/TransformerTextBundle/"
            }
        }

    * Rebuild autoload:
    .. code-block:: terminal

        $ composer dump-autoload

    * Enable bundle in your project:
    .. code-block:: php

        return [
            ...
            TransformerTextBundle\TransformerTextBundle::class => ['all' => true],
            ...
        ];

##Using
Execute the next command to begin the interactive dialogue with the transformer:
.. code-block:: terminal

    $ bin/console text:process