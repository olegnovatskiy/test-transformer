<?php

namespace TransformerTextBundle;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TransformerTextBundle\DependencyInjection\TransformersExtension;

/**
 * Class TransformerTextBundle
 */
class TransformerTextBundle extends Bundle
{


    /**
     * Returns the container extension that should be implicitly loaded.
     *
     * @return ExtensionInterface|null The default extension or null if there is none
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new TransformersExtension();
        }

        return $this->extension;
    }
}
