<?php

namespace TransformerTextBundle\Interfaces;

/**
 * Interface TextTransformerInterface
 */
interface TextTransformerInterface
{


    /**
     * Remove all HTML tags from text.
     *
     * @param string $text
     *
     * @return string
     */
    public function removeHtml(string $text): string;

    /**
     * Wrap out a text to paragraphs.
     *
     * @param string $text
     *
     * @return string
     */
    public function wrapOutParagraph(string $text): string;
}
