<?php

namespace TransformerTextBundle\Tests\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TransformerTextBundle\DependencyInjection\TransformerService;

/**
 * Class TransformerServiceTest
 */
class TransformerServiceTest extends WebTestCase
{


    /**
     * Testing the method of removing HTML tags from text.
     */
    public function testRemovingHtml()
    {
        self::bootKernel();
        $testingService = self::$container->get(TransformerService::class);

        $testText = '<p>Hello world</p>';

        $this->assertEquals(
            'Hello world',
            $testingService->removeHtml($testText),
            'Assert 1: Text are not equal.'
        );

        $this->assertNotEquals(
            'Not equal text',
            $testingService->removeHtml($testText),
            'Assert 2: Text are equal.'
        );

        $this->assertEquals(
            'Hi, Oleh. Where are you?',
            $testingService->removeHtml(
                '<p>Hi, Oleh.</p> <p>Where are you?</p>'
            ),
            'Assert 3: Text are not equal.'
        );
    }

    /**
     * Testing the method of wrapping the text lines to paragraphs.
     */
    public function testWrapping()
    {
        self::bootKernel();
        $testingService = self::$container->get(TransformerService::class);

        $testText = 'Hello world 1 \n Hello world 2';

        $this->assertEquals(
            '<p>Hello world 1</p><p>Hello world 2</p>',
            $testingService->wrapOutParagraph($testText),
            'Assert 1: Text are not equal.'
        );

        $this->assertNotEquals(
            'Hello world 1 \n Hello world 2',
            $testingService->wrapOutParagraph($testText),
            'Assert 2: Text are equal.'
        );

        $this->assertEquals(
            '<p></p><p>Hello world 1</p><p>Hello world 2</p>',
            $testingService->wrapOutParagraph(
                '\nHello world 1 \n Hello world 2'
            ),
            'Assert 3: Text are not equal.'
        );
    }
}
