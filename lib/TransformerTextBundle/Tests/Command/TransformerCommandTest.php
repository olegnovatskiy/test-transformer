<?php

namespace TransformerTextBundle\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class TransformerCommandTest
 */
class TransformerCommandTest extends KernelTestCase
{


    /**
     * Test executing of command.
     */
    public function testExecute()
    {
        $kernel      = static::createKernel();
        $application = new Application($kernel);

        $command       = $application->find('text:process');
        $commandTester = new CommandTester($command);

        $commandTester->setInputs(['0']);

        $commandTester->execute(['command' => $command->getName()]);
        $this->assertContains('Exit!', $commandTester->getDisplay());

        $commandTester->setInputs(
            [
                '3',
                '2',
                '1',
                '\nHello world! \n Where are you?\n',
            ]
        );

        $commandTester->execute(['command' => $command->getName()]);
        $this->assertContains(
            '<p></p><p>Hello world!</p><p>Where are you?</p><p></p>',
            $commandTester->getDisplay()
        );
    }
}
